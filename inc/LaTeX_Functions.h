/**
 * @file LaTeX_Functions.h
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Functions to draw formula symbols
 * @version 1.0
 * @date 2022-05-08
 *
 * Remarks:
*/
#pragma once

#include <Image.hpp>
#include "Latex.h"
#include <memory>
#include <vector>

const uint16_t AM_FORMULA_TYPES = 12;
extern Formula Formula_Types[AM_FORMULA_TYPES];


class LaTeX_Functions {
public:
  static bool _frac(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _frac_size(Formula_Block* fb);
  static bool _sqrt(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _sqrt_size(Formula_Block* fb);
  static bool _pow(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _pow_size(Formula_Block* fb);
  static bool _index(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _index_size(Formula_Block* fb);
  static bool _index_pow(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _index_pow_size(Formula_Block* fb);
  static bool _bracket_round(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _bracket_round_size(Formula_Block* fb);
  static bool _bracket_square(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _bracket_square_size(Formula_Block* fb);
  static bool _sum(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _sum_size(Formula_Block* fb);
  static bool _matrix(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _matrix_size(Formula_Block* fb);
  static bool _dot(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _dot_size(Formula_Block* fb);
  static bool _cdot(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _cdot_size(Formula_Block* fb);
  static bool _int(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y);
  static void _int_size(Formula_Block* fb);
};