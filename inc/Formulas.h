/**
 * @file Formulas.h
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief
 * @version 1.0
 * @date 2022-05-08
 *
 * Remarks:
*/
#pragma once
#include <memory>
#include <vector>
#include <string.h>
#include "Image.hpp"

class Formula_Argument;
class Formula_Block;

class Formula {
public:
  uint8_t am_arguments;
  char command[6];
  uint8_t command_Length;
  bool (*function)(std::shared_ptr<Graphics::Image>, Formula_Block*, uint16_t, uint16_t);
  void (*size_function)(Formula_Block*);
  uint8_t fontsize_change;

  Formula(char _command[], uint8_t _am_arguments, bool (*_function)(std::shared_ptr<Graphics::Image>, Formula_Block*, uint16_t, uint16_t), void (*_size_function)(Formula_Block*), uint8_t _fontsize_change) {
    this->am_arguments = _am_arguments;
    strcpy(this->command, _command);
    command_Length = strlen(this->command);
    this->function = _function;
    this->size_function = _size_function;
    this->fontsize_change = _fontsize_change;
  }
};

class Dimensions {
public:
  // Distance between Top of Screen and Mid of the following char
  uint16_t Ymid;

  // Width of this block
  uint16_t Width;

  // Height of this block
  uint16_t Height;

  // Fontsize of this block
  Graphics::FontSize FontSize;
};

class Formula_Block : public Dimensions {
public:
  typedef enum {
    NUL = 0, // -> Not now defined
    PRINT, // -> string
    COMMAND, // -> argument(s)
  } Type;

  std::vector<Formula_Argument> arguments;
  std::vector<char> string;
  Type type;
  Formula* formula;

  std::vector<Formula_Argument>* parent;
  uint8_t parent_index;
  Formula_Argument* Get_Parent() { return &(*parent)[parent_index]; }

  Formula_Block();
  Formula_Block(std::vector<Formula_Argument>* _Parent, uint8_t _parent_index);

private:

};

class Formula_Argument : public Dimensions {
public:
  std::vector<Formula_Block> parts;

  std::vector<Formula_Block>* parent;
  uint8_t parent_index;
  Formula_Block* Get_Parent() { return &(*parent)[parent_index]; }

  Formula_Argument();
  Formula_Argument(std::vector<Formula_Block>* _Parent, uint8_t _parent_index);
};