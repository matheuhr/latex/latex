/**
 * @file Latex.h
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Converter from Tex-Commands to picture
 * @version 1.0
 * @date 2022-05-08
 *
 * Remarks:
*/
#pragma once

#include "Image.hpp"
#include <string.h>
#include <memory>
#include <vector>
#include "Formulas.h"

#define START_FORMULA_Y 30
#define END_FORMULA_Y   300
#define FORMULA_MID_Y   ((END_FORMULA_Y + START_FORMULA_Y) / 2)

class LaTeX {
public:
  /**
   *  @brief  Gibt ein Image einer per LaTeX interpretierten Formel zurück
   *  @param  str  Zu interpretierende Formel
   *  @return  An Image of the interpreted formula
   *
   * Aufbau Formel (Parameter: str):
   * - Zahlen (Int, Double mit Punkt): 0123456789.
   * - Grundrechenarten: + - *
   * - Brüche: \frac{...}{...}
   * - Wurzel: \sqrt{...}
   * - Klammerausdrücke: \({...} | Achtung, werden immer mit geschweifter Klammer geschlossen!
   * - Klammerausdrücke: \[{...} | Achtung, werden immer mit geschweifter Klammer geschlossen!
   * - Potenzen (Exponent muss mit geschweiften Klammern geklammert werden!): [...]^{...}
   * - %-Zeichen: %%
   * - Index: ..._{...}
   * - Index und Potenz gleichzeitig: \ip{index}{potenz}
   * - Integral: \int{Index}{Ende}{Inhalt}
   * - Summe: \sum{Index}{Ende}{Inhalt}
   * - Sonderzeichen: (MYSQL: CONCAT("...", UNHEX("7F"), "...")), verfügbare Fonts siehe fonts.h
   *    '°' - 0x7F
   *    'ß' - 0x80
   *    'unendlich' - 0x81
   * - Zeilenumbruch: \n
   * - Matrix: \matrix{Anzahl Zeilen n}{Anzahl Spalten m}{Zelle 1/1}{Zelle 1/...}{Zelle 1/m}{Zelle .../1}[...]{Zelle n/m} | n und m müssen positive Integer < 10 sein!
   * - Punkt/zeit. Ableitung: \dot{...}
   * - Multiplikationssymbol: \cdot
   *
   * Wichtig: Formel darf nicht mit Zeilenumbruch enden!
   */
  static std::shared_ptr<Graphics::Image> Interpret(const std::vector<char>& str, uint16_t Height, uint16_t Width, uint8_t size = -1);
  static void Calc_Dimensions(Formula_Argument* fa);
  static bool Print_TeX(std::shared_ptr<Graphics::Image> img, Formula_Argument* fa, uint16_t X, uint16_t Y);
};

/*
Ignoriert:
  0  - 9   - Befehle
  11 - 31  - Befehle
  127      - DEL

Direkter Print :
  32 - 91  - SP ! " # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [
  93       - ]
  96 - 126 - ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~

Sonderbefehl:
  10 - LF
  92 - \
  94 - ^
  95 - _
*/