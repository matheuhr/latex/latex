/**
 * @file Formulas.cpp
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief
 * @version 1.0
 * @date 2022-05-08
 *
 * Remarks:
*/
#include "Formulas.h"

Formula_Block::Formula_Block() {
  Width = 0;
  Height = 0;
  type = Type::NUL;
}

Formula_Block::Formula_Block(std::vector<Formula_Argument>* _Parent, uint8_t _parent_index) {
  Width = 0;
  Height = 0;
  type = Type::NUL;
  parent = _Parent;
  parent_index = _parent_index;
  FontSize = Get_Parent()->FontSize;
}

Formula_Argument::Formula_Argument() {
  Width = 0;
  Height = 0;
}

Formula_Argument::Formula_Argument(std::vector<Formula_Block>* _Parent, uint8_t _parent_index) {
  Width = 0;
  Height = 0;
  parent = _Parent;
  parent_index = _parent_index;
  Formula_Block* fb = Get_Parent();
  FontSize = fb->FontSize - fb->formula->fontsize_change;
}