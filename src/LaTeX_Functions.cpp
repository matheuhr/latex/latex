/**
 * @file LaTeX_Functions.cpp
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Functions to draw formula symbols
 * @version 1.0
 * @date 2022-05-08
 *
 * Remarks:
*/
#include "LaTeX_Functions.h"

#include <array>
#include <cstdint>

constexpr std::array<std::uint8_t, Graphics::NumberOfFonts> DotSizes = { 1, 1, 1, 2, 2, 3, 4, 5, 6, 7, 7 };

std::uint8_t FontSizeToDotSize(Graphics::FontSize fontSize, std::size_t add = 0) {
  const auto fontSizeUint = fontSize.toUint();
  if (fontSizeUint + add >= DotSizes.size())
    return DotSizes.back();
  else if (fontSizeUint + add < 0)
    return DotSizes.front();
  else
    return DotSizes[fontSizeUint + add];
}

Formula Formula_Types[AM_FORMULA_TYPES] = {
  /*Formula((char*)"LaTeX-Code", _am_arguments, Draw Function-Pointer, Size-Function Pointe, _fontsize_change), */
  Formula((char*)"\\frac", 2, &LaTeX_Functions::_frac, &LaTeX_Functions::_frac_size, 0),
  Formula((char*)"\\sqrt", 1, &LaTeX_Functions::_sqrt, &LaTeX_Functions::_sqrt_size, 0),
  Formula((char*)"^", 1, &LaTeX_Functions::_pow,&LaTeX_Functions::_pow_size, 1),
  Formula((char*)"_", 1, &LaTeX_Functions::_index, &LaTeX_Functions::_index_size, 1),
  Formula((char*)"\\ip", 2, &LaTeX_Functions::_index_pow, &LaTeX_Functions::_index_pow_size, 1),
  Formula((char*)"\\(", 1, &LaTeX_Functions::_bracket_round, &LaTeX_Functions::_bracket_round_size, 0),
  Formula((char*)"\\[", 1, &LaTeX_Functions::_bracket_square, &LaTeX_Functions::_bracket_square_size, 0),
  Formula((char*)"\\sum", 3, &LaTeX_Functions::_sum, &LaTeX_Functions::_sum_size, 1),
  Formula((char*)"\\matr", 83, &LaTeX_Functions::_matrix, &LaTeX_Functions::_matrix_size, 0), /* Maximum: 9x9 */
  Formula((char*)"\\dot", 1, &LaTeX_Functions::_dot, &LaTeX_Functions::_dot_size, 0),
  Formula((char*)"\\cdot", 0, &LaTeX_Functions::_cdot, &LaTeX_Functions::_cdot_size, 0),
  Formula((char*)"\\int", 3, &LaTeX_Functions::_int, &LaTeX_Functions::_int_size, 1)
};

bool LaTeX_Functions::_frac(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  LaTeX::Print_TeX(img, &fb->arguments[0], X + 2 * FontSizeToDotSize(fb->FontSize) + (fb->Width - fb->arguments[0].Width - 2 * FontSizeToDotSize(fb->FontSize)) / 2, Y - FontSizeToDotSize(fb->FontSize) / 2 - 2 - (fb->arguments[0].Height - fb->arguments[0].Ymid));
  LaTeX::Print_TeX(img, &fb->arguments[1], X + 2 * FontSizeToDotSize(fb->FontSize) + (fb->Width - fb->arguments[1].Width - 2 * FontSizeToDotSize(fb->FontSize)) / 2, Y + FontSizeToDotSize(fb->FontSize) / 2 + 2 + fb->arguments[1].Ymid);
  img->DrawLineAngle(X + 2 * FontSizeToDotSize(fb->FontSize), Y - fb->Ymid + fb->arguments[0].Height + 2, 90, fb->Width - 2 * FontSizeToDotSize(fb->FontSize) - 1, FontSizeToDotSize(fb->FontSize));
  return true;
}

void LaTeX_Functions::_frac_size(Formula_Block* fb) {
  fb->Height = fb->arguments[0].Height + 4 + FontSizeToDotSize(fb->FontSize) + fb->arguments[1].Height;
  fb->Width = (fb->arguments[0].Width > fb->arguments[1].Width ? fb->arguments[0].Width : fb->arguments[1].Width) + 2 * FontSizeToDotSize(fb->FontSize);
  fb->Ymid = fb->arguments[0].Height + (4 + FontSizeToDotSize(fb->FontSize)) / 2;
}

bool LaTeX_Functions::_sqrt(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  LaTeX::Print_TeX(img, &fb->arguments[0],
    X + 2.5 * FontSizeToDotSize(fb->FontSize) + 0.54596 * fb->arguments[0].Height - 2 * (fb->arguments[0].FontSize.toUint() == 1),
    Y - fb->Ymid + fb->arguments[0].Ymid + FontSizeToDotSize(fb->FontSize) + 1);

  Y = Y - fb->Ymid;
  uint16_t XS = X;
  uint16_t YS = Y + fb->Height * 3 / 5;
  uint16_t XE = XS + 2 * FontSizeToDotSize(fb->FontSize);
  uint16_t YE = YS;
  img->DrawLine(XS, YS, XE, YE, FontSizeToDotSize(fb->FontSize)); // Waagerechter Haenkel

  XS = XE; YS = YE;
  YE = Y + fb->Height - 1;
  XE += (YE - YS) / 2.74748; // tan(70°)=2.74748
  img->DrawLine(XS, YS, XE, YE, FontSizeToDotSize(fb->FontSize)); // Diagonale nach unten

  XS = XE; YS = YE;
  YE = Y;
  XE += (YS - YE) / 2.74748; // tan(70°)=2.74748
  img->DrawLine(XS, YS, XE, YE, FontSizeToDotSize(fb->FontSize)); // Diagonale nach oben

  XS = XE; YS = YE;
  XE = X + fb->Width - FontSizeToDotSize(fb->FontSize); // tan(70°)=2.74748
  img->DrawLine(XS, YS, XE, YE, FontSizeToDotSize(fb->FontSize)); // Waagerechte oben

  XS = XE; YS = YE;
  YE += fb->arguments[0].Height / 4; // tan(70°)=2.74748
  img->DrawLine(XS, YS, XE, YE, FontSizeToDotSize(fb->FontSize)); // Haenkel rechts
  return true;
}

void LaTeX_Functions::_sqrt_size(Formula_Block* fb) {
  fb->Height = fb->arguments[0].Height + FontSizeToDotSize(fb->FontSize) + 1;
  //fb->Width = 2 * FontSizeToDotSize(fb->FontSize) + 0.54596 * fb->arguments[0].Height + fb->arguments[0].Width + FontSizeToDotSize(fb->FontSize) + 1;
  fb->Width = 2 * FontSizeToDotSize(fb->FontSize) + (fb->Height * 3 / 5 - 1) / 2.74748 + (fb->Height - 1 - FontSizeToDotSize(fb->FontSize)) / 2.74748 + fb->arguments[0].Width + 1 + FontSizeToDotSize(fb->FontSize) - 1;
  fb->Ymid = fb->Height * 1 / 2 + FontSizeToDotSize(fb->FontSize);
}

bool LaTeX_Functions::_pow(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  LaTeX::Print_TeX(img, &fb->arguments[0], X, Y - fb->Ymid + fb->arguments[0].Ymid);
  return true;
}

void LaTeX_Functions::_pow_size(Formula_Block* fb) {
  fb->Height = fb->arguments[0].Height;
  fb->Width = fb->arguments[0].Width;
  fb->Ymid = fb->Height;
}

bool LaTeX_Functions::_index(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  LaTeX::Print_TeX(img, &fb->arguments[0], X, Y - fb->Ymid + fb->arguments[0].Ymid);
  return true;
}

void LaTeX_Functions::_index_size(Formula_Block* fb) {
  fb->Height = fb->arguments[0].Height;
  fb->Width = fb->arguments[0].Width;
  fb->Ymid = 0;
}

bool LaTeX_Functions::_index_pow(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  LaTeX::Print_TeX(img, &fb->arguments[0], X, Y - fb->Ymid + fb->arguments[0].Ymid);
  LaTeX::Print_TeX(img, &fb->arguments[1], X, Y + fb->arguments[1].Ymid);
  return true;
}

void LaTeX_Functions::_index_pow_size(Formula_Block* fb) {
  fb->Height = fb->arguments[0].Height + fb->arguments[1].Height;
  fb->Width = fb->arguments[0].Width > fb->arguments[1].Width ? fb->arguments[0].Width : fb->arguments[1].Width;
  fb->Ymid = fb->arguments[0].Height;
}

bool LaTeX_Functions::_bracket_round(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  LaTeX::Print_TeX(img, &fb->arguments[0], X + 2 * FontSizeToDotSize(fb->FontSize), Y - fb->Ymid + fb->arguments[0].Ymid);
  Y = Y - fb->Ymid;
  uint8_t radius = 3 * FontSizeToDotSize(fb->FontSize);
  img->DrawCirclePart(X + radius, Y + radius, radius, 270, 360, FontSizeToDotSize(fb->FontSize));
  img->DrawLine(X, Y + radius, X, Y + fb->arguments[0].Height - radius - 1, FontSizeToDotSize(fb->FontSize));
  img->DrawCirclePart(X + radius, Y + fb->arguments[0].Height - radius - 1, radius, 180, 270, FontSizeToDotSize(fb->FontSize));

  img->DrawCirclePart(X + fb->Width - FontSizeToDotSize(fb->FontSize) - radius, Y + radius, radius, 0, 90, FontSizeToDotSize(fb->FontSize));
  img->DrawLine(X + fb->Width - FontSizeToDotSize(fb->FontSize), Y + radius, X + fb->Width - FontSizeToDotSize(fb->FontSize), Y + fb->arguments[0].Height - radius - 1, FontSizeToDotSize(fb->FontSize));
  img->DrawCirclePart(X + fb->Width - FontSizeToDotSize(fb->FontSize) - radius, Y + fb->arguments[0].Height - radius - 1, radius, 90, 180, FontSizeToDotSize(fb->FontSize));
  return true;
}

void LaTeX_Functions::_bracket_round_size(Formula_Block* fb) {
  fb->Height = fb->arguments[0].Height;
  fb->Width = fb->arguments[0].Width + 4 * FontSizeToDotSize(fb->FontSize);
  fb->Ymid = fb->arguments[0].Ymid;
}

bool LaTeX_Functions::_bracket_square(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  LaTeX::Print_TeX(img, &fb->arguments[0], X + 2 * FontSizeToDotSize(fb->FontSize), Y - fb->Ymid + fb->arguments[0].Ymid + 2 * FontSizeToDotSize(fb->FontSize));
  Y = Y - fb->Ymid;
  uint8_t radius = 3 * FontSizeToDotSize(fb->FontSize);
  img->DrawLine(X, Y, X + radius, Y, FontSizeToDotSize(fb->FontSize));
  img->DrawLine(X, Y, X, Y + fb->Height - FontSizeToDotSize(fb->FontSize), FontSizeToDotSize(fb->FontSize));
  img->DrawLine(X, Y + fb->Height - FontSizeToDotSize(fb->FontSize), X + radius, Y + fb->Height - FontSizeToDotSize(fb->FontSize), FontSizeToDotSize(fb->FontSize));

  img->DrawLine(X + fb->Width - FontSizeToDotSize(fb->FontSize) - radius, Y, X + fb->Width - FontSizeToDotSize(fb->FontSize), Y, FontSizeToDotSize(fb->FontSize));
  img->DrawLine(X + fb->Width - FontSizeToDotSize(fb->FontSize), Y, X + fb->Width - FontSizeToDotSize(fb->FontSize), Y + fb->Height - FontSizeToDotSize(fb->FontSize) + 1, FontSizeToDotSize(fb->FontSize));
  img->DrawLine(X + fb->Width - FontSizeToDotSize(fb->FontSize) - radius, Y + fb->Height - FontSizeToDotSize(fb->FontSize), X + fb->Width - FontSizeToDotSize(fb->FontSize), Y + fb->Height - FontSizeToDotSize(fb->FontSize), FontSizeToDotSize(fb->FontSize));
  return true;
}

void LaTeX_Functions::_bracket_square_size(Formula_Block* fb) {
  fb->Height = fb->arguments[0].Height + 3 * FontSizeToDotSize(fb->FontSize);
  fb->Width = fb->arguments[0].Width + 4 * FontSizeToDotSize(fb->FontSize);
  fb->Ymid = fb->Height / 2;
}

bool LaTeX_Functions::_sum(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  uint8_t radius = 3 * FontSizeToDotSize(fb->FontSize);
  LaTeX::Print_TeX(img, &fb->arguments[2], X + radius + 1, Y);
  Y = Y - fb->Ymid;
  LaTeX::Print_TeX(img, &fb->arguments[1], X, Y + fb->arguments[1].Ymid);
  LaTeX::Print_TeX(img, &fb->arguments[0], X, Y + fb->Height - fb->arguments[0].Height + fb->arguments[0].Ymid);

  img->DrawLine(X, Y + fb->arguments[1].Height, X + 2 * radius, Y + fb->arguments[1].Height, FontSizeToDotSize(fb->FontSize));
  img->DrawLine(X, Y + fb->arguments[1].Height, X + radius, Y + fb->arguments[1].Height + fb->arguments[2].Height / 2, FontSizeToDotSize(fb->FontSize));
  img->DrawLine(X, Y + fb->Ymid - fb->arguments[2].Ymid + fb->arguments[2].Height, X + radius, Y + fb->arguments[1].Height + fb->arguments[2].Height / 2, FontSizeToDotSize(fb->FontSize));
  img->DrawLine(X, Y + fb->Ymid - fb->arguments[2].Ymid + fb->arguments[2].Height, X + 2 * radius, Y + fb->Ymid - fb->arguments[2].Ymid + fb->arguments[2].Height, FontSizeToDotSize(fb->FontSize));
  return true;
}

void LaTeX_Functions::_sum_size(Formula_Block* fb) {
  fb->Height = fb->arguments[0].Height + fb->arguments[1].Height + fb->arguments[2].Height + FontSizeToDotSize(fb->FontSize);
  fb->Width = 4 * FontSizeToDotSize(fb->FontSize) + fb->arguments[2].Width;
  if (fb->arguments[0].Width > fb->Width)
    fb->Width = fb->arguments[0].Width;
  if (fb->arguments[1].Width > fb->Width)
    fb->Width = fb->arguments[1].Width;
  fb->Ymid = fb->arguments[1].Height + fb->arguments[2].Ymid;
}

bool LaTeX_Functions::_matrix(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  //\\matrix{ Anzahl Zeilen n }{Anzahl Spalten m} {Zelle 1 / 1} {Zelle 1 / ...} {Zelle 1 / m} {Zelle ... / 1}[...]{ Zelle n / m
  uint8_t n = fb->arguments[0].parts[0].string[0] - '0';
  uint8_t m = fb->arguments[1].parts[0].string[0] - '0';
  uint16_t max_width[m] = {};
  uint16_t max_height[n] = {};
  for (uint8_t y = 0; y < n; y++) {
    for (uint8_t x = 0; x < m; x++) {
      if (max_width[x] < fb->arguments[2 + y * m + x].Width)
        max_width[x] = fb->arguments[2 + y * m + x].Width;
      if (max_height[y] < fb->arguments[2 + y * m + x].Height)
        max_height[y] = fb->arguments[2 + y * m + x].Height;
    }
  }

  uint16_t Pos_Y = Y - fb->Ymid;
  for (uint8_t y = 0; y < n; y++) {
    uint16_t Pos_X = X;
    for (uint8_t x = 0; x < m; x++) {
      Formula_Argument* fa = &fb->arguments[2 + y * m + x];
      LaTeX::Print_TeX(img, fa, Pos_X + (max_width[x] - fa->Width) / 2, Pos_Y + max_height[y] / 2 - fa->Height / 2 + fa->Ymid);
      Pos_X += max_width[x] + FontSizeToDotSize(fb->FontSize);
    }
    Pos_Y += max_height[y] + FontSizeToDotSize(fb->FontSize);
  }
  return true;
}

void LaTeX_Functions::_matrix_size(Formula_Block* fb) {
  uint8_t n = fb->arguments[0].parts[0].string[0] - '0';
  uint8_t m = fb->arguments[1].parts[0].string[0] - '0';
  fb->Height = (n - 1) * FontSizeToDotSize(fb->FontSize);
  fb->Width = (m - 1) * FontSizeToDotSize(fb->FontSize);
  for (uint8_t x = 0; x < m; x++) {
    uint16_t cur_column = 0;
    for (uint8_t y = 0; y < n; y++) {
      if (cur_column < fb->arguments[2 + y * m + x].Width)
        cur_column = fb->arguments[2 + y * m + x].Width;
    }

    fb->Width += cur_column;
  }

  for (uint8_t y = 0; y < n; y++) {
    uint16_t cur_row = 0;
    for (uint8_t x = 0; x < m; x++) {
      if (cur_row < fb->arguments[2 + y * m + x].Height)
        cur_row = fb->arguments[2 + y * m + x].Height;
    }

    fb->Height += cur_row;
  }

  fb->Ymid = fb->Height / 2;
}

bool LaTeX_Functions::_dot(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  LaTeX::Print_TeX(img, &fb->arguments[0], X, Y);
  img->DrawPoint(X + fb->Width / 2 + FontSizeToDotSize(fb->FontSize), Y - fb->Ymid + FontSizeToDotSize(fb->FontSize), FontSizeToDotSize(fb->FontSize));
  return true;
}

void LaTeX_Functions::_dot_size(Formula_Block* fb) {
  fb->Width = fb->arguments[0].Width;
  fb->Height = fb->arguments[0].Height + 2 * FontSizeToDotSize(fb->FontSize);
  fb->Ymid = fb->Height - fb->arguments[0].Height / 2;
}

bool LaTeX_Functions::_cdot(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  img->DrawPoint(X + fb->Width / 2, Y, FontSizeToDotSize(fb->FontSize, 1));
  return true;
}

void LaTeX_Functions::_cdot_size(Formula_Block* fb) {
  fb->Width = 4 * FontSizeToDotSize(fb->FontSize, 1);
  fb->Height = 1 + 2 * FontSizeToDotSize(fb->FontSize, 1);
  fb->Ymid = fb->Height / 2;
}

bool LaTeX_Functions::_int(std::shared_ptr<Graphics::Image> img, Formula_Block* fb, uint16_t X, uint16_t Y) {
  const auto fontDotSize = FontSizeToDotSize(fb->FontSize);
  const uint8_t radius = 3 * fontDotSize;
  LaTeX::Print_TeX(img, &fb->arguments[2], X + 2 * radius + 1, Y); // Argument
  Y = Y - fb->Ymid; // Y is at top now
  LaTeX::Print_TeX(img, &fb->arguments[1], X, Y + fb->arguments[1].Ymid); // Upper Limit
  LaTeX::Print_TeX(img, &fb->arguments[0], X, Y + fb->Height - fb->arguments[0].Height + fb->arguments[0].Ymid); // Downer Limit
  DEBUG_OUTPUT printf("Integraldotsize is: %u\n", fontDotSize);
  const auto y_upper = Y + fb->arguments[1].Height + radius;
  const auto y_downer = Y + fb->arguments[1].Height + fb->arguments[2].Height + radius;
  img->DrawCirclePart(X + 2 * radius - fontDotSize, y_upper, radius, 270, 360, fontDotSize); // Upper circle
  img->DrawLine(X + radius - fontDotSize, y_upper, X + radius - fontDotSize, y_downer, fontDotSize);
  img->DrawCirclePart(X - 1, y_downer, radius, 90, 180, fontDotSize); // Downer circle

  DEBUG_OUTPUT printf("Draw Integral:\nfontsize: %u, radius: %u, y_upper: %u, y_downer: %u\n X: %u, Y: %u, Width: %u, Height: %u\n", fontDotSize, radius, y_upper, y_downer, X, Y, fb->Width, fb->Height);
  return true;
}

void LaTeX_Functions::_int_size(Formula_Block* fb) {
  const uint8_t radius = 3 * FontSizeToDotSize(fb->FontSize);
  fb->Height = fb->arguments[0].Height + fb->arguments[1].Height + fb->arguments[2].Height + 2 * radius;
  fb->Width = radius + fb->arguments[2].Width;
  if (fb->arguments[0].Width > fb->Width)
    fb->Width = fb->arguments[0].Width;
  if (fb->arguments[1].Width > fb->Width)
    fb->Width = fb->arguments[1].Width;
  fb->Ymid = fb->arguments[1].Height + fb->arguments[2].Ymid + radius;
}

