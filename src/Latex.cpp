/**
 * @file Latex.cpp
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Converter from Tex-Commands to picture
 * @version 1.0
 * @date 2022-05-08
 *
 * Remarks:
*/
#include "Latex.h"
#include <stdlib.h>
#include "Debug.h"
#include "Image.hpp"
#include "Fonts.hpp"
#include <memory>
#include <vector>
#include <string.h>
#include "LaTeX_Functions.h"

// Debug Functions
void Print_Strcture(Formula_Argument* list);
void Print_List(Formula_Argument* list);

std::shared_ptr<Graphics::Image> LaTeX::Interpret(const std::vector<char>& str_start, uint16_t Height, uint16_t Width, uint8_t size) {
  auto str = str_start.begin();
  std::vector<Formula_Block> first_block_list;
  first_block_list.push_back(Formula_Block());
  Formula_Block& first_block = first_block_list[0];
  first_block.formula = &Formula_Types[1]; // Just someone with 1 argument is important for the dimension calculation
  first_block.arguments.push_back(Formula_Argument(&first_block_list, 0));
  Formula_Argument& first_arg = first_block_list[0].arguments[0];
  Formula_Argument* cur_arg = &first_block.arguments[0];
  cur_arg->parts.push_back(Formula_Block(&first_block.arguments, 0));
  Formula_Block* cur_block = &cur_arg->parts.back();
  cur_arg->FontSize.fontSize_ = static_cast<Graphics::FontSize::FS>(size == (uint8_t)(-1) ? Graphics::NumberOfFonts - 1 : size); // TODO
  cur_block->FontSize = cur_arg->FontSize;

  // Analysing LaTeX String

  while (str != str_start.end()) {
    bool next_is_char = false;

    // Analyze next char
    if (*str == '\\' || *str == '^' || *str == '_') { // Command
      Formula* f = nullptr;
      for (uint8_t i = 0; i < AM_FORMULA_TYPES; i++) {
        if (strncmp(Formula_Types[i].command, (const char*)&*str, Formula_Types[i].command_Length) == 0) {
          f = &Formula_Types[i];
          break;
        }
      }
      if (f == nullptr) {
        if (DEBUG(D_LATEX_INTERPRET, DEBUG_DEPTH_NONE)) {
          DEBUG_OUTPUT printf("LaTeX-Interpret: Command unknown: '%s' -> Special Char\n", &*str);
        }
        return nullptr;
      } else {
        if (cur_block->type != Formula_Block::Type::NUL) {
          cur_arg->parts.push_back(Formula_Block(&cur_arg->Get_Parent()->arguments, cur_arg->Get_Parent()->arguments.size() - 1));
          cur_block = &(cur_arg->parts.back());
        }
        cur_block->formula = f;
        cur_block->type = Formula_Block::Type::COMMAND;
        str += cur_block->formula->command_Length - 1;
      }
    } else if (*str == '{') { // begin new block
      if (cur_block->arguments.size() < cur_block->formula->am_arguments) {
        cur_block->arguments.push_back(Formula_Argument(&cur_arg->parts, cur_arg->parts.size() - 1));
        cur_arg = &(cur_block->arguments.back());
        cur_arg->parts.push_back(Formula_Block(&cur_block->arguments, cur_block->arguments.size() - 1));
        cur_block = &(cur_arg->parts.back());
        cur_block->type = Formula_Block::Type::NUL;
      } else {
        if (DEBUG(D_LATEX_INTERPRET, DEBUG_DEPTH_NONE)) {
          DEBUG_OUTPUT printf("LaTeX-Interpret: Too many arguments! '%s' -> Special Char\n", &*str);
        }
        return nullptr;
      }
    } else if (*str == '}') { // end last block
      Calc_Dimensions(cur_arg);
      cur_block = cur_arg->Get_Parent();
      cur_arg = cur_block->Get_Parent();
    } else {
      next_is_char = true;
    }


    if (next_is_char) { // Print
      if (cur_block->type != Formula_Block::Type::PRINT) {
        if (cur_block->type == Formula_Block::Type::COMMAND)
          cur_arg->parts.push_back(Formula_Block(&cur_arg->Get_Parent()->arguments, cur_arg->Get_Parent()->arguments.size() - 1));
        cur_block = &cur_arg->parts.back();
        cur_block->type = Formula_Block::Type::PRINT;
      }
      cur_block->string.push_back(*str);
    }
    str++;
  }
  Calc_Dimensions(&first_arg);

  if (DEBUG(D_LATEX_INTERPRET, DEBUG_DEPTH_DEEP)) {
    DEBUG_OUTPUT printf("Interpreted: '");
    Print_List(&first_arg);
    DEBUG_OUTPUT printf("'\nPrinting Structure:\n");
    Print_Strcture(&first_arg);
    DEBUG_OUTPUT printf("\n");
  }

  // Resizing if too big
  std::vector<Formula_Argument*> all;
  all.push_back(&first_arg);

  uint8_t k = 0;
  uint8_t k_end = 0;
  uint8_t k_new_end = 1;
  while (k_end != k_new_end) {
    k_end = k_new_end;
    while (k < k_end) {
      for (uint8_t i = 0; i < all[k]->parts.size(); i++) {
        for (uint8_t j = 0; j < all[k]->parts[i].arguments.size(); j++) {
          all.push_back(&all[k]->parts[i].arguments[j]);
          k_new_end++;
        }
      }
      k++;
    }
  }
  if (DEBUG(D_LATEX_INTERPRET, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("\nAmount of arguments: %d\n", all.size());
  if (DEBUG(D_LATEX_INTERPRET, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("Sum Size: %d x %d\n", first_arg.Height, first_arg.Width);

  while (first_arg.Width > Width || first_arg.Height > Height) {
    if (first_arg.FontSize == Graphics::FontSize::S8) {
      if (DEBUG(D_LATEX_INTERPRET, DEBUG_DEPTH_NONE)) {
        DEBUG_OUTPUT printf("LaTeX-Interpret: Too big formular! '%s' -> Special Char\n", &*str);
      }
      return nullptr;
    }
    if (DEBUG(D_LATEX_INTERPRET, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("Resizing...\n");

    for (uint8_t i = all.size() - 1; i < (uint8_t)-1; i--) {
      for (uint8_t j = 0; j < all[i]->parts.size(); j++)
        all[i]->parts[j].FontSize--;
      all[i]->FontSize--;
      Calc_Dimensions(all[i]);
    }
    if (DEBUG(D_LATEX_INTERPRET, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("Sum Size: %d x %d\n", first_arg.Height, first_arg.Width);
  }

  if (DEBUG(D_LATEX_INTERPRET, DEBUG_DEPTH_DEEP)) {
    DEBUG_OUTPUT printf("\nPrinting Structure:\n");
    Print_Strcture(&first_arg);
  }

  std::shared_ptr<Graphics::Image> img(new Graphics::Image(first_arg.Width, first_arg.Height));
  img->Clear();
  Print_TeX(img, &first_arg, 0, first_arg.Ymid);
  return img;
}

// Ymid gibt an, wie viele Pixel von oben die Mitte des Bildes entfernt sein soll
bool LaTeX::Print_TeX(std::shared_ptr<Graphics::Image> img, Formula_Argument* fa, uint16_t X, uint16_t Y) {
  for (uint8_t j = 0; j < fa->parts.size(); j++) {
    if (fa->parts[j].type == Formula_Block::Type::PRINT) {
      if (DEBUG(D_LATEX_PRINT_TEX, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("Print_Tex: X=%u; Y=%u; FontSize=%u; FontWidth=%u; FontHeight=%u\n", X, Y, fa->parts[j].FontSize, fa->parts[j].Width, fa->parts[j].Height);
      img->DrawStringV(X, Y - fa->parts[j].Ymid, &fa->parts[j].string, Graphics::Fonts::getInstance().getFont(fa->parts[j].FontSize));
    } else {
      fa->parts[j].formula->function(img, &fa->parts[j], X, Y);
    }
    X += fa->parts[j].Width;
  }
  return true;
}

void LaTeX::Calc_Dimensions(Formula_Argument* fa) {
  uint8_t am_parts = fa->parts.size();

  fa->Width = 0;
  fa->Height = 0;
  fa->Ymid = 0;

  for (uint8_t i = 0; i < am_parts; i++) {
    if (fa->parts[i].type == Formula_Block::Type::COMMAND)
      fa->parts[i].formula->size_function(&fa->parts[i]);
    else {
      fa->parts[i].Width = Graphics::Fonts::GetWidth(fa->parts[i].FontSize, fa->parts[i].string);
      fa->parts[i].Height = Graphics::Fonts::GetHeight(fa->parts[i].FontSize);
      fa->parts[i].Ymid = fa->parts[i].Height / 2;
    }

    fa->Width += fa->parts[i].Width;

    if (fa->Ymid <= fa->parts[i].Ymid && fa->Height - fa->Ymid <= fa->parts[i].Height - fa->parts[i].Ymid)
      fa->Height = fa->parts[i].Height;
    else if (fa->Ymid > fa->parts[i].Ymid && fa->Height - fa->Ymid > fa->parts[i].Height - fa->parts[i].Ymid)
      fa->Height = fa->Height;
    else if (fa->Ymid < fa->parts[i].Ymid)
      fa->Height = fa->Height - fa->Ymid + fa->parts[i].Ymid;
    else if (fa->Ymid > fa->parts[i].Ymid)
      fa->Height = fa->parts[i].Height - fa->parts[i].Ymid + fa->Ymid;

    if (fa->Ymid < fa->parts[i].Ymid) { // img beginnt höher als imgnext
      fa->Ymid = fa->parts[i].Ymid;
    }
  }

  Formula_Block* fb = fa->Get_Parent();
  if (fb->type == Formula_Block::Type::COMMAND && fb->arguments.size() == fb->formula->am_arguments)
    fb->formula->size_function(fb);
}

// Debug Functions
void Print_List(Formula_Argument* list) {
  for (uint8_t i = 0; i < list->parts.size(); i++) {
    if (list->parts[i].type == Formula_Block::Type::PRINT) {
      for (uint8_t j = 0; j < list->parts[i].string.size(); j++)
        DEBUG_OUTPUT printf("%c", list->parts[i].string[j]);
    } else {
      DEBUG_OUTPUT printf("%s", list->parts[i].formula->command);
      for (uint8_t j = 0; j < list->parts[i].arguments.size(); j++) {
        DEBUG_OUTPUT printf("{");
        Print_List(&list->parts[i].arguments[j]);
        DEBUG_OUTPUT printf("}");
      }
    }
  }
}

void Print_Strcture(Formula_Argument* list) {
  DEBUG_OUTPUT printf("Argument: %d x %d, Ymid: %d, Font: %d\n", list->Height, list->Width, list->Ymid, list->FontSize);
  for (uint8_t i = 0; i < list->parts.size(); i++) {
    if (list->parts[i].type == Formula_Block::Type::PRINT) {
      DEBUG_OUTPUT printf("Part: \"");
      for (uint8_t j = 0; j < list->parts[i].string.size(); j++)
        DEBUG_OUTPUT printf("%c", list->parts[i].string[j]);

      DEBUG_OUTPUT printf("\" (%d x %d, Ymid: %d, Font: %d)\n", list->parts[i].Height, list->parts[i].Width, list->parts[i].Ymid, list->parts[i].FontSize);
    } else {
      DEBUG_OUTPUT printf("Part: \"%s\": %d x %d, Ymid: %d, Font: %d\n", list->parts[i].formula->command, list->parts[i].Height, list->parts[i].Width, list->parts[i].Ymid, list->parts[i].FontSize);
      for (uint8_t j = 0; j < list->parts[i].arguments.size(); j++) {
        Print_Strcture(&list->parts[i].arguments[j]);
      }
    }
  }
}
